#include <iostream>
#include <string>
#include <iomanip>
class dayType
{

private:
    std::string Days[7];
    std::string currentDay;
    int numDays;
public:
    void setDay(std::string newDay);
    void printDay() const;
    int retDay(int& day);
    int retnextDay(int day);
    int retprevDay(int day) const;
    int retcalcDay(int day, int numDays);

    dayType()
    {
        Days[0] = "Sunday";
        Days[1] = "Monday";
        Days[2] = "Tuesday";
        Days[3] = "Wednesday";
        Days[4] = "Thursday";
        Days[5] = "Friday";
        Days[6] = "Saturday";
        currentDay = Days[0];
        numDays = 0;
    };

};
void showSelections();
int main()
{
    dayType userDay;
    int currentDay;
    int addDays;
    int test;
    std::string day;

    do
    {
        test = 0;
        showSelections();
        std::cin >> currentDay;
        std::cout << std::endl;
        if (currentDay == 0)
        {
            userDay.setDay("Sunday");
        }
        else if (currentDay == 1)
        {
            userDay.setDay("Monday");
        }
        else if (currentDay == 2)
        {
            userDay.setDay("Tuesday");
        }
        else if (currentDay == 3)
        {
            userDay.setDay("Wednesday");
        }
        else if (currentDay == 4)
        {
            userDay.setDay("Thursday");
        }
        else if (currentDay == 5)
        {
            userDay.setDay("Friday");
        }
        else if (currentDay == 6)
        {
            userDay.setDay("Saturday");
        }
        else if (currentDay == 9)
        {
            std::cout << "Exiting...";
            return 0;
        }
        else
        {
            std::cout << "Not a valid choice." << std::endl;
            test = -37;
        }
    }

    while (test < 0);
    userDay.printDay();
    std::cout << std::endl;
    userDay.retDay(currentDay);
    std::cout << std::endl;
    userDay.retnextDay(currentDay);
    std::cout << std::endl;
    userDay.retprevDay(currentDay);
    std::cout << std::endl;
    std::cout << "Enter the number of days to add: " << std::endl;
    std::cin >> addDays;
    std::cout << std::endl;
    userDay.retcalcDay(currentDay, addDays);
    std::cout << std::endl;
    std::cout << std::endl << std::endl;
    system("pause");
    return 0;
}
void dayType::setDay(std::string newDay)
{
    currentDay= newDay;
}
void dayType::printDay() const
{
    std::cout<<"day chosen is"<<currentDay<<std::endl;
}
int dayType::retDay(int& day)
{
    return day;
}
int dayType::retnextDay(int day)
{
    day=day+1;
    if (day>6)
        day=day%7;
    switch (day)
    {
        case 0: std::cout << "The next day is Sunday";
            break;
        case 1: std::cout << "The next day is Monday";
            break;
        case 2: std::cout << "The next day is Tuesday";
            break;
        case 3: std::cout << "The next day is Wednesday";
            break;
        case 4: std::cout << "The next day is Thursday";
            break;
        case 5: std::cout << "The next day is Friday";
            break;
        case 6: std::cout << "The next day is Saturday";
            break;
    }
    std::cout<<std::endl;
    return day;
}
int dayType::retprevDay(int day) const
{
    day = day-1;
    switch (day)
    {
        case -1: std::cout << "The previous day is Saturday";
            break;
        case 0: std::cout << "The previous day is Sunday";
            break;
        case 1: std::cout << "The previous day is Monday";
            break;
        case 2: std::cout << "The previous day is Tuesday";
            break;
        case 3: std::cout << "The previous day is Wednesday";
            break;
        case 4: std::cout << "The previous day is Thursday";
            break;
        case 5: std::cout << "The previous day is Friday";
            break;
        default: std::cout << "The previous day is Saturday.";
    }
    std::cout << std::endl;
    return day;
}
int dayType::retcalcDay(int addDays, int numDays)
{
    addDays = addDays + numDays;
    if (addDays > 6)
        addDays = addDays % 7;
    switch(addDays)
    {
        case 0: std::cout << "The calculated day is Sunday.";
            break;
        case 1: std::cout << "The calculated day is Monday.";
            break;
        case 2: std::cout << "The calculated day is Tuesday.";
            break;
        case 3: std::cout << "The calculated day is Wednesday.";
            break;
        case 4: std::cout << "The calculated day is Thursday.";
            break;
        case 5: std::cout << "The calculated day is Friday.";
            break;
        case 6: std::cout << "The calculated day is Saturday.";
            break;
        default: std::cout << "Not valid choice.";
    }
    std::cout << std::endl;
    return addDays;
}

void showSelections()
{
    std::cout << "*****Please enter a day of the week*****" << std::endl;
    std::cout << "0 for Sunday" << std::endl;
    std::cout << "1 for Monday" << std::endl;
    std::cout << "2 for Tuesday" << std::endl;
    std::cout << "3 for Wednesday" << std::endl;
    std::cout << "4 for Thursday" << std::endl;
    std::cout << "5 for Friday" << std::endl;
    std::cout << "6 for Saturday" << std::endl;
    std::cout << "9 to exit" << std::endl;


}